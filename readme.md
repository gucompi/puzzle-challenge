# Project Setup and Usage Guide

This guide provides instructions on how to set up and run the project. Please follow the steps below to get started.

## Prerequisites

Before running the project, please ensure that you have the following installed:

- MongoDB server (running and accessible)
- Node.js (v12 or higher)

## Configuration

1. Open the `backend/.env` file.
2. Set the `MONGODB_URI` variable to the MongoDB connection URL. For example:

MONGODB_URI=mongodb://localhost:27017/myapp


## Backend Setup

1. Open a terminal and navigate to the project's `backend` directory:
cd backend

2. Install the required dependencies:
npm install

3. Start the backend server:
npm run start

## Frontend Setup

1. Open another terminal and navigate to the project's `frontend` directory:
cd frontend

2. Install the required dependencies:
npm install

3. Start the frontend development server:

## User Registration

To create a new user, follow these steps:

1. Consume the registration service by sending a request to `/register`.
2. Provide the necessary information to complete the registration process.

## Logging In

After successfully registering, you can log in to the web application by following these steps:

1. Access the web application at `http://localhost:3000`.
2. Use the login functionality provided on the website.

Congratulations! You have now set up and logged in to the project. You are ready to explore and utilize its features.

Please note that these instructions assume a local development environment. Make sure to adjust any configuration details as needed for your specific setup.