import jwtDecode from 'jwt-decode';

export function getUserFromToken(token: string) {
  let decoded : any = jwtDecode(token);
  if(decoded && decoded.user) {
    return decoded.user;
  }
  return "";
}

export function getCartFromToken(token: string) {
  let decoded : any = jwtDecode(token);
  if(decoded && decoded.cart) {
    return decoded.cart;
  }
  return "";
}

