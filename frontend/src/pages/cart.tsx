import Layout from '@/layouts/layout';
import React, { useEffect, useState } from 'react';

interface CartItem {
  id: string;
  price: number;
  quantity: number;
title: string;
}

interface Order {
  userId: string;
  products: CartItem[];
  date: Date;
  status: 'Active' | 'Completed';
  total: number;
}

const MyCartPage: React.FC = () => {
  const [activeOrder, setActiveOrder] = useState<Order | null>(null);

  useEffect(() => {
    // Fetch the active order from your API
    fetch('http://localhost:5000/orders-active', {
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then((data) => setActiveOrder(data));
  }, []);


  const handleCompleteOrder = () => {
    if (activeOrder) {
      fetch(`http://localhost:5000/orders/finish-order`, {
        method: 'POST',
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('token'),
          'Content-Type': 'application/json',
        },
      })
        .then((response) => response.json())
        .then((data) => {
          // Handle the response as needed
          console.log('Order completed:', data);
          localStorage.setItem('cart', JSON.stringify(data));
          window.location.reload();

        })
        .catch((error) => {
          // Handle any errors
          console.error('Error completing order:', error);
        });
    }
  };

  if (!activeOrder) {
    return <div>Loading...</div>;
  }

  return (
    <Layout>
      <div className="bg-gray-100 min-h-screen">
        <div className="container mx-auto py-8">
          <h1 className="text-3xl font-bold text-center mb-8">My Cart</h1>
          {!activeOrder || !activeOrder.products || activeOrder.products.length === 0 ? (
            <p className="text-center text-gray-500">Your cart is empty.</p>
          ) : (
            <div>
              <ul className="space-y-4">
                {activeOrder.products.map((item) => (
                  <li
                    key={item.id}
                    className="bg-white rounded shadow-md px-8 py-6 flex items-center justify-between"
                  >
                    <div>
                      <h2 className="text-xl font-bold mb-2">{item.id} - {item.title}</h2>
                      <p className="text-gray-600">Price: ${item.price}</p>
                    </div>
                  </li>
                ))}
              </ul>
              <button
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 mt-4"
                onClick={handleCompleteOrder}
              >
                Complete Order
              </button>
            </div>
          )}
        </div>
      </div>
    </Layout>
  );
};

export default MyCartPage;
