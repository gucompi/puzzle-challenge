"use client"

import Search from '@/app/components/search';
import Layout from '@/layouts/layout';
import React, { useEffect, useState } from 'react';
import NavbarClient from '../app/components/navbar';
import ProductCard from '../app/components/productcard';

interface Rating {
  rate: number;
  count: number;
}

interface Product {
  id: number;
  title: string;
  price: number;
  description: string;
  category: string;
  image: string;
  rating: Rating;
}

export default function Home() {
  const [products, setProducts] = useState<Product[]>([]);

  useEffect(() => {
    fetch('http://localhost:5000/products')
      .then((response) => response.json())
      .then((data) => setProducts(data));
  }, []);

  return (
    <Layout>
      <div className="flex flex-wrap justify-around">
        <Search setProducts={setProducts}></Search>
        {products.map((product) => (
          <ProductCard key={product.id} product={product} />
        ))}
      </div>
    </Layout>
  );
}
