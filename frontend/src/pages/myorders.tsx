import Layout from '@/layouts/layout';
import React, { useEffect, useState } from 'react';

interface Order {
  _id: string;
  status: string;
  products: OrderItem[];
}

interface OrderItem {
  _id: string;
  title: string;
  price: number;
  quantity: number;
}

const MyOrdersPage: React.FC = () => {
  const [orders, setOrders] = useState<Order[]>([]);

  useEffect(() => {
    // Fetch the user's orders from your API
    fetch('http://localhost:5000/orders', {
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setOrders(data);
        console.log(data);
      });
  }, []);

  if (orders.length === 0) {
    return (
      <Layout>
        <div className="bg-gray-100 min-h-screen">
          <div className="container mx-auto py-8">
            <h1 className="text-3xl font-bold text-center mb-8">My Orders</h1>
            <p className="text-center text-gray-500">You have no orders yet.</p>
          </div>
        </div>
      </Layout>
    );
  }

  return (
    <Layout>
      <div className="bg-gray-100 min-h-screen">
        <div className="container mx-auto py-8">
          <h1 className="text-3xl font-bold text-center mb-8">My Orders</h1>
          <div className="grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-3">
            {orders.map((order: Order) => (
              <div
                key={order._id}
                className="bg-white rounded-lg shadow-md p-6 hover:shadow-lg"
              >
                <h2 className="text-xl font-bold mb-4">Order #{order._id}</h2>
                <p className="text-gray-600">Status: {order.status}</p>
                <ul className="mt-4">
                  {order.products.map((item: OrderItem) => (
                    <li
                      key={item._id}
                      className="flex items-center justify-between"
                    >
                      <div>
                        <h3 className="font-bold">{item.title}</h3>
                        <p>Price: ${item.price}</p>
                      </div>
                    </li>
                  ))}
                </ul>
              </div>
            ))}
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default MyOrdersPage;
