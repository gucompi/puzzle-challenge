import React, { useState } from 'react';
import Layout from '../layouts/layout';
import { useRouter } from 'next/router';
import { getUserFromToken, getCartFromToken} from '@/utils.js/token';

const LoginPage: React.FC = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const handleLogin = async (event: React.FormEvent) => {
    event.preventDefault();

    // Display loading state
    setLoading(true);

    try {
      // Send POST request to API endpoint
      const response = await fetch('http://localhost:5000/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ username, password }),
      });

      if (response.ok) {
        // Retrieve token from the API response
        const data = await response.json();
        const token = data.token;

        localStorage.setItem('token', token);
        localStorage.setItem('user', JSON.stringify(getUserFromToken(token)));
        localStorage.setItem('cart', JSON.stringify(getCartFromToken(token)));
        
        // Clear form inputs
        setUsername('');
        setPassword('');
        setError('');
        router.push('/');

      } else {
        // Handle API error response
        setError('Invalid username or password');
      }
    } catch (error) {
      console.error('Error occurred during login:', error);
      setError('An error occurred during login');
    } finally {
      // Clear loading state
      setLoading(false);
    }
  };

  return (
    <Layout>
      <div className="flex flex-col items-center justify-center min-h-screen bg-gray-100">
        <div className="bg-white p-8 rounded shadow-md">
          <h2 className="text-2xl font-bold mb-6">Login</h2>
          <form onSubmit={handleLogin} className="space-y-4">
            <div>
              <label htmlFor="username" className="block mb-1 font-semibold text-gray-700">
                Username
              </label>
              <input
                type="text"
                id="username"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                className="w-full px-4 py-2 border border-gray-300 rounded focus:outline-none focus:border-teal-500"
                placeholder="Enter your username"
              />
            </div>
            <div>
              <label htmlFor="password" className="block mb-1 font-semibold text-gray-700">
                Password
              </label>
              <input
                type="password"
                id="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                className="w-full px-4 py-2 border border-gray-300 rounded focus:outline-none focus:border-teal-500"
                placeholder="Enter your password"
              />
            </div>
            {error && <p className="text-red-500">{error}</p>}
            <button
              type="submit"
              className="w-full bg-teal-500 text-white font-semibold py-2 px-4 rounded hover:bg-teal-600"
              disabled={loading}
            >
              {loading ? 'Logging in...' : 'Log In'}
            </button>
          </form>
        </div>
      </div>
    </Layout>
  );
};

export default LoginPage;
