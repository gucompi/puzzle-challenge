import React, { useState, useEffect } from 'react';

const Search: React.FC<any> = ({ setProducts }: any) => {
  const [searchText, setSearchText] = useState('');
  const [selectedFilters, setSelectedFilters] = useState<{ [key: string]: string }>({});

  const filters: string[] = ['price', 'description', 'category']; // Replace with your array of filter options

  const handleSearchTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value);
  };

  const handleFilterSelection = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedFilter = event.target.value;
    setSelectedFilters((prevFilters) => ({
      ...prevFilters,
      [selectedFilter]: '',
    }));
  };

  const handleFilterTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const selectedFilter:any = event.target.dataset.filter;
    const filterText = event.target.value;
    setSelectedFilters((prevFilters) => ({
      ...prevFilters,
      [selectedFilter]: filterText,
    }));
  };

  const fetchData = async () => {
    try {
      let query = 'title=' + searchText;
      for (const filter in selectedFilters) {
        if (selectedFilters[filter]) {
          query += `&${filter}=${selectedFilters[filter]}`;
        }
      }
      const response = await fetch(`http://localhost:5000/products?${query}`);
      const data = await response.json();
      setProducts(data);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  return (
    <div className="p-4 bg-gray-200 rounded-lg">
      <input
        type="text"
        placeholder="Enter search text"
        value={searchText}
        onChange={handleSearchTextChange}
        className="w-full py-2 px-4 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500"
      />
      <select
        onChange={handleFilterSelection}
        className="mt-2 py-2 px-4 rounded-md bg-white border border-gray-300 focus:outline-none focus:ring-2 focus:ring-blue-500"
      >
        <option value="">Add filter</option>
        {filters.map((filter) => (
          <option key={filter} value={filter}>
            {filter}
          </option>
        ))}
      </select>
      {Object.keys(selectedFilters).map((filter) => (
        <input
          key={filter}
          type="text"
          placeholder={`Enter ${filter}`}
          value={selectedFilters[filter]}
          onChange={handleFilterTextChange}
          data-filter={filter}
          className="mt-2 py-2 px-4 rounded-md bg-white border border-gray-300 focus:outline-none focus:ring-2 focus:ring-blue-500"
        />
      ))}
      <div className="mt-2">
        <span className="mr-2 text-gray-600">Selected Filters:</span>
        {Object.keys(selectedFilters).map((filter) =>
          selectedFilters[filter] ? (
            <span key={filter} className="inline-block px-2 py-1 bg-blue-500 text-white rounded-full mr-2">
              {filter}
            </span>
          ) : null
        )}
      </div>
      <button className="mt-4 py-2 px-4 bg-blue-500 text-white rounded-md hover:bg-blue-600" onClick={fetchData}>
        Search
      </button>
    </div>
  );
};

export default Search;
