import React, { useState } from 'react';

interface Product {
  id: number;
  title: string;
  price: number;
  description: string;
  category: string;
  image: string;
}

interface ProductCardProps {
  product: Product;
}

const ProductCard: React.FC<ProductCardProps> = ({ product }) => {
  const [hovered, setHovered] = useState(false);

  const handleAddToCart = async () => {
    // Perform the 'addToCart' action
    console.log('Adding to cart:', product.title);
    //request http//localhost:5000/orders/:id/add-product
    //body: {product: product}
    //getorderId from localstorage
    const orderId = JSON.parse(window.localStorage.getItem('cart') || '{}')._id;
    await fetch(`http://localhost:5000/orders/${orderId}/add-product`, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'), // 'Bearer ' is needed for JWT
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ product }),
    });
  };

  return (
    <div
      className="max-w-sm rounded border border-gray-300 overflow-hidden shadow-lg m-4 bg-gray-100"
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
    >
      <img className="w-full" src={product.image} alt={product.title} />
      <div className="px-6 py-4">
        <div className="font-bold text-xl mb-2">{product.title}</div>
        <p className="text-gray-700 text-base">{product.description}</p>
      </div>
      <div className="px-6 pt-4 pb-2">
        <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">
          #{product.category}
        </span>
        <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">
          ${product.price}
        </span>
      </div>
      {hovered && (
        <div className="px-6 py-2 text-center bg-gray-200">
          <button
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
            onClick={handleAddToCart}
          >
            Add to Cart
          </button>
        </div>
      )}
    </div>
  );
};

export default ProductCard;
