import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

interface NavbarProps {
  user: string | null;
}

const NavbarClient: React.FC<NavbarProps> = ({ user, cart }: any) => {
  const router = useRouter();

  const handleLogout = () => {
    // Clear local storage items
    localStorage.removeItem('cart');
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    
    // Redirect to '/'
    router.push('/');
    if(router.pathname == "/") router.reload();
  };

  return (
    <nav className="flex items-center justify-between flex-wrap bg-teal-500 p-6">
      <div className="flex items-center flex-shrink-0 text-white mr-6">
        <Link legacyBehavior href="/">
          <a className="font-semibold text-xl tracking-tight">My Store</a>
        </Link>
        <Link legacyBehavior href="/myorders">
          <a className="ml-4 text-white">My Orders</a>
        </Link>
      </div>
      <div className="block lg:hidden">
        <button className="flex items-center px-3 py-2 border rounded text-teal-200 border-teal-400 hover:text-white hover:border-white">
          <svg className="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
            <title>Menu</title>
            <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
          </svg>
        </button>
      </div>
      <div className="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
        <div className="text-sm lg:flex-grow">
          {/* Add more navigation links here */}
        </div>
        <div>
          {user && user.username ? (
            <>
              <span className="text-white mr-3">Hello, {user.username}</span>
              {cart && cart.products && (
                <Link legacyBehavior href="/cart">
                  <a className="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-teal-500 hover:bg-white mt-4 lg:mt-0">
                    Cart
                  </a>
                </Link>
              )}
              <button
                className="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-teal-500 hover:bg-white mt-4 lg:mt-0"
                onClick={handleLogout}
              >
                Logout
              </button>
            </>
          ) : (
            <Link legacyBehavior href="/login">
              <a className="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-teal-500 hover:bg-white mt-4 lg:mt-0">
                Log In
              </a>
            </Link>
          )}
        </div>
      </div>
    </nav>
  );
};

export default NavbarClient;
