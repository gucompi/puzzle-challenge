import '../app/globals.css'
import React from 'react';
import Navbar from '../app/components/navbar';
import { useEffect, useState } from "react"

const Layout: React.FC = ({ children }: any) => {
  //string or null
  let [user, setUser] = useState({});
  let [cart, setCart] = useState({});

  useEffect(() => {
    let value;
    value = JSON.parse(window.localStorage.getItem('user') || "{}");
    setUser(value);

    let cart = JSON.parse(window.localStorage.getItem('cart') || "{}");
    setCart(cart);

  }, [])


  return (
    <div>
      <Navbar user={user} cart={cart}/>
      <main>{children}</main>
    </div>
  );
};

export default Layout;
