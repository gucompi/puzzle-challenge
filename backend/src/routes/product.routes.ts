import { Express } from 'express';
import { getProducts } from '../controllers/product.controller';

const setupProductRoutes = (app: Express) => {
  app.get('/products', getProducts);
};

export default setupProductRoutes;
