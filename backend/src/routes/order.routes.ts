import { Express } from 'express';
import { 
  getOrders,
  addProductToOrder,
  getActiveOrder,
  finishOrderByIdAndCreateNew
} from '../controllers/order.controller';
import { authMiddleware } from '../middleware/auth';

const setupOrderRoutes = (app: Express) => {
  app.get('/orders', authMiddleware as any, getOrders as any);
  app.post('/orders/:id/add-product', authMiddleware as any, addProductToOrder as any);
  app.post('/orders/finish-order',authMiddleware as any, finishOrderByIdAndCreateNew as any)
  app.get('/orders-active', authMiddleware as any, getActiveOrder as any);
};

export default setupOrderRoutes;
