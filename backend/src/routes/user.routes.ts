import { Express } from 'express';
import { login, register } from '../controllers/user.controller';

const setupUserRoutes = (app: Express) => {
  app.post('/login', login);
  app.post('/register', register);

};

export default setupUserRoutes;
