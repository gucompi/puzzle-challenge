import { Express } from 'express';
import setupUserRoutes from './user.routes';
import setupProductRoutes from './product.routes';
import setupOrderRoutes from './order.routes';

const setupRoutes = (app: Express) => {
  setupUserRoutes(app);
  setupProductRoutes(app);
  setupOrderRoutes(app);  
};

export default setupRoutes;
