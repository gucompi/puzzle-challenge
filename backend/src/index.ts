import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
import connectDB from './database';
import setupRoutes from './routes/app.routes';

dotenv.config();

const app = express();

app.use(cors());
app.use(express.json());

// Connect to MongoDB
connectDB();

// Setup routes
setupRoutes(app);

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
