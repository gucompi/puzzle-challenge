import { Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import { RequestWithUser } from '../types/customRequests';

interface TokenPayload {
  id: string;
  iat: number;
  exp: number;
}

export const authMiddleware = (
  req: RequestWithUser,
  res: Response,
  next: NextFunction
) => {
  const { authorization } = req.headers;

  if (!authorization) {
    res.sendStatus(401);
    return next(new Error('No authorization header'));
  }

  const token = authorization.replace('Bearer ', '');

  try {
    const data:any = jwt.verify(token, process.env.JWT_SECRET as string);
    const { id } = data.user as TokenPayload;
    req.userId = id;

    return next();
  } catch {
    res.sendStatus(401);
    return next(new Error('Invalid token'));
  }
}