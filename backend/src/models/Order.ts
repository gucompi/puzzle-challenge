import mongoose, { Document, Schema } from 'mongoose';

export interface IOrder extends Document {
  userId: string;
  products: Array<{ productId: string, price: number }>;
  date: Date;
  status: 'Active' | 'Completed';
  total: number;
}

const OrderSchema: Schema = new Schema({
  userId: { type: String, required: true },
  products: [Number],
  date: { type: Date, default: Date.now },
  status: { type: String, enum: ['Active', 'Completed'], default: 'Active' },
  total: { type: Number}
});

export default mongoose.model<IOrder>('Order', OrderSchema);
