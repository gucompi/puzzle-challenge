import { Response, NextFunction } from 'express';
import { RequestWithUser } from '../types/customRequests';
import Order from '../models/Order';
import axios from 'axios';


export const getOrders = async (req: RequestWithUser, res: Response) => {
  try {
    let orders:any = await Order.find({ userId: req.userId });

    const response = await axios.get('https://fakestoreapi.com/products');
    let products = response.data;

  
    let new_orders = orders.map((order:any) => {
      let productsNew = order.products.map((product:any) => {
        const _product = products.find((p:any) => p.id === product);
        return _product;
      }).filter((p:any)=>p);
      order._doc.products = productsNew;
      return order;
    });

    res.json(new_orders);
  } catch (err:any) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
};
export const addProductToOrder = async (req: RequestWithUser, res: Response) => {
  try {
    const order = await Order.findById(req.params.id);
    if (!order) {
      return res.status(404).json({ message: 'Order not found' });
    }   
    if (order.userId !== req.userId) {
      return res.status(403).json({ message: 'You do not have permission to update this order' });
    }
    const productId = req.body.product.id;
    const updatedOrder = await Order.findByIdAndUpdate(req.params.id, { $push: { products: productId } }, { new: true });
    res.json(updatedOrder);
  } catch (error:any) {
    res.status(500).json({ message: error.message });
  }
};
export const getActiveOrder = async (req: RequestWithUser, res: Response) => {
  try {
    const order:any = await Order.findOne({
      userId: req.userId,
      status: 'Active',
    });
    if (!order) {
      throw {
        message: 'Order not found',
        status: 404,
      }
    }

    const response = await axios.get('https://fakestoreapi.com/products');
    let products = response.data;

    let productsNew = order.products.map((product:any) => {
      const _product = products.find((p:any) => p.id === product);
      return _product;
    }).filter((p:any)=>p);
  
    order._doc.products = productsNew;
    

    res.json(order);
  } catch (error:any) {
    res.status(500).json({ message: error.message });
  }
};

export const finishOrderByIdAndCreateNew = async (req: RequestWithUser, res: Response) => {
  try {
    const order:any = await Order.findOne({
      userId: req.userId,
      status: 'Active',
    });
    if (!order) {
      throw {
        message: 'Order not found',
        status: 404,
      }
    }
    if (!order) {
      return res.status(404).json({ message: 'Order not found' });
    }
    if (order.userId !== req.userId) {
      return res.status(403).json({ message: 'You do not have permission to update this order' });
    }
    const updatedOrder = await Order.findByIdAndUpdate(order._id, { status: 'Completed' }, { new: true });
    const newOrder = new Order({
      userId: req.userId,
      products: [],
      date: new Date(),
      status: 'Active',
    });
    await newOrder.save();
    res.json(newOrder);
  } catch (error:any) {
    res.status(500).json({ message: error.message });
  }
};
  