import { Request, Response } from 'express';
import axios from 'axios';

export const getProducts = async (req: Request, res: Response) => {
  try {
    const response = await axios.get('https://fakestoreapi.com/products');
    let products = response.data;

    // Filter by name
    if (req.query.title) {
      products = products.filter((product: any) =>
        product.title.toLowerCase().includes((req.query.title as string).toLowerCase())
      );
    }

    // Filter by category
    if (req.query.category) {
      products = products.filter((product: any) =>
        product.category.toLowerCase() === (req.query.category as string).toLowerCase()
      );
    }

    // Filter by price
    if (req.query.price) {
      products = products.filter((product: any) =>
        product.price <= Number(req.query.price)
      );
    }

    res.json(products);
  } catch (err:any) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
};
