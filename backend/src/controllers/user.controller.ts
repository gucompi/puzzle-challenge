import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import User from '../models/User';
import Order from '../models/Order';

export const getActiveOrder = async (userId : string) => {
  try {
    const order = await Order.findOne({
      userId: userId,
      status: 'Active',
    });
    if (!order) {
      throw {
        message: 'Order not found',
        status: 404,
      }
    }
    return order;
  } catch (error:any) {
    throw {
      message: error.message || 'Server Error',
      status: error.status || 500,
    }
  }
};
export const login = async (req: Request, res: Response) => {
  const { username, password } = req.body;

  try {
    // Check if user exists
    const user = await User.findOne({ username });
    if (!user) {
      return res.status(400).json({ message: 'Invalid Credentials' });
    }

    // Check if password matches
    const isMatch = await user.matchPassword(password);
    if (!isMatch) {
      return res.status(400).json({ message: 'Invalid Credentials' });
    }

    let  cart;
    try{
      cart = await getActiveOrder(user.id);
    } catch(e:any){
      console.log(e);
      if(e.status === 404){
        const newOrder = new Order({
          userId: user.id,
          products: [],
          date: new Date(),
          status: 'Active',
        });
        cart = await newOrder.save();
      }
    }

    // Sign JWT
    const payload = {
      user: {
        id: user.id,
        username
      },
      cart
    };

    jwt.sign(payload, process.env.JWT_SECRET!, { expiresIn: 360000 }, (err, token) => {
      if (err) throw err;
      res.json({ token });
    });
  } catch (err:any) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
};

export const register = async (req: Request, res: Response) => {
  const { username, password } = req.body;

  try {
    // Check if username is taken
    let user = await User.findOne({ username });
    if (user) {
      return res.status(400).json({ message: 'User already exists' });
    }

    user = new User({
      username,
      password,
    });

    // Save user
    await user.save();

    // Sign JWT
    const payload = {
      user: {
        id: user.id,
      },
    };

    jwt.sign(payload, process.env.JWT_SECRET!, { expiresIn: 360000 }, (err, token) => {
      if (err) throw err;
      res.json({ token });
    });
  } catch (err:any) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
};